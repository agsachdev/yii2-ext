Groovy First Ext
================
TESTING

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist myself/yii2-myself "*"
```

or add

```
"myself/yii2-myself": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \myself\foobar\AutoloadExample::widget(); ?>```